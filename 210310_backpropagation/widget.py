import backpropagation as bp
from ipywidgets import (widgets, interactive_output, IntSlider,
                        FloatSlider, RadioButtons, Layout, Box, VBox)


def plot_net(Pattern, Activation, n_obs, n_iter, learning_rate, seed):

    net = bp.Network(Pattern(n_obs),
                     Activation=Activation,
                     learning_rate=learning_rate,
                     seed=seed)
    print("Entraînement du réseau...")
    net.train(n_iter=n_iter, verbose=False)
    net.plot_animation(net.iter)
    bp.plt.show()


patterns = ['Diamond', 'Circle', 'Square']
pattern_classes = bp.Diamond, bp.Circle, bp.Square
pattern_buttons = widgets.RadioButtons(
    options=list(zip(patterns, pattern_classes)),
    description='Pattern:',
    disabled=False
)

activation_functions = ['Sigmoid', 'ReLU', 'tanh']
activation_function_classes = bp.Sigmoid, bp.ReLU, bp.Tanh
activation_functions_buttons = widgets.RadioButtons(
    options=list(zip(activation_functions, activation_function_classes)),
    description='Activ. func.:',
    disabled=False
)

n_obs_slider = IntSlider(min=100, max=10000, step=100, value=500,
                         continuous_update=False,
                         description=r'\(n_{obs}:\)')

n_iter_slider = IntSlider(min=100, max=50000, step=100, value=5000,
                          continuous_update=False,
                          description=r'\(n_{iter}:\)')

learning_rate_text = widgets.BoundedFloatText(
    value=0.001, min=0., max=1., step=0.0001,
    description=r'\(\lambda :\)',
    disabled=False
)

seed_text = widgets.BoundedIntText(
    value=1241, min=0, max=10000, step=1,
    description='Graine',
    disabled=False
)

kw = dict(Pattern=pattern_buttons,
          Activation=activation_functions_buttons,
          n_obs=n_obs_slider,
          n_iter=n_iter_slider,
          learning_rate=learning_rate_text,
          seed=seed_text)
out = interactive_output(plot_net, kw)

box_layout = Layout(display='flex',
                    flex_flow='row',
                    align_items='stretch',
                    width='100%')
box_1 = Box(children=[pattern_buttons,
                      activation_functions_buttons, seed_text],
            layout=box_layout)
box_2 = Box(children=[n_obs_slider, n_iter_slider,
                      learning_rate_text],
            layout=box_layout)
ui = VBox([box_1, box_2])

display(ui, out)
