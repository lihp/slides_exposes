# Exposés du Groupe de Travail *Machine learning* de l'IRMA

## Exécuter les notebooks

### En ligne

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fml%2Fslides_exposes.git/master)

### En local

`python 3.8` est actuellement nécessaire pour tensorflow.
On peut l'installer facilement avec [Conda](https://www.anaconda.com/products/individual) ou [Miniconda](https://docs.conda.io/en/latest/miniconda.html) (plus léger).

Créer et activer un environnement virtuel avec  :

```bash
python3.8 -m venv .venv
source .venv/bin/activate
```

Installer les dépendances :

```bash
pip install -r requirements.txt
```

Lancer un serveur Jupyter :

```bash
jupyter-notebook
```
